Title: DebConf17 starts today in Montreal
Slug: dc17-starts
Date: 2017-08-06 16:00
Author: Laura Arjona Reina
Tags: debconf17, debconf
Status: published

![DebConf17 logo](|filename|/images/800px-Dc17logo.png)

[DebConf17](https://debconf17.debconf.org/), the 18th annual
Debian Conference, is taking place in Montreal, Canada
from August 6 to August 12, 2017.

Debian contributors from all over the world have come together at
[Collège Maisonneuve](https://wiki.debconf.org/wiki/DebConf17/Venue)
during the preceding week for DebCamp (focused on individual work
and team sprints for in-person collaboration developing Debian),
and the Open Day on August 5th (with presentations and workshops
of interest to a wide audience).

Today the main conference starts with nearly 400 attendants
and over 120 activities scheduled,
including 45- and 20-minute talks and team meetings,
workshops, a job fair, talks from invited speakers,
as well as a variety of other events.

The full schedule at
[https://debconf17.debconf.org/schedule/](https://debconf17.debconf.org/schedule/)
is updated every day, including activities planned ad-hoc
by attendees during the whole conference.

If you want to engage remotely, you can follow the
[**video streaming**](https://debconf17.debconf.org/live-streaming/)
of the events happening in the three talk rooms:
Buzz (the main auditorium), Rex, and Bo,
or join the conversation about what is happening
in the talk rooms:
  [**#debconf17-buzz**](https://webchat.oftc.net/?channels=#debconf17-buzz),
  [**#debconf17-rex**](https://webchat.oftc.net/?channels=#debconf17-rex) and
  [**#debconf17-bo**](https://webchat.oftc.net/?channels=#debconf17-bo),
and the BoF (discussions) rooms:  [**#debconf17-potato**](https://webchat.oftc.net/?channels=#debconf17-potato)
and [**#debconf17-woody**](https://webchat.oftc.net/?channels=#debconf17-woody)
(all those channels in the OFTC IRC network).

DebConf is committed to a safe and welcome environment for all participants.
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml)
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf17.debconf.org/sponsors/)
to support DebConf17, particularly our Platinum Sponsors
[**Savoir-Faire Linux**](https://www.savoirfairelinux.com/),
[**Hewlett Packard Enterprise**](http://www.hpe.com/engage/opensource),
and [**Google**](https://google.com/).
