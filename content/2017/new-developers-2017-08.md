Title: New Debian Developers and Maintainers (July and August 2017)
Slug: new-developers-2017-08
Date: 2017-09-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * Ross Gammon (rossgammon)
  * Balasankar C (balasankarc)
  * Roland Fehrenbacher (rfehren)
  * Jonathan Cristopher Carter (jcc)

The following contributors were added as Debian Maintainers in the last two months:

  * José Gutiérrez de la Concha
  * Paolo Greppi
  * Ming-ting Yao Wei
  * Boyuan Yang
  * Paul Hardy
  * Fabian Wolff
  * Moritz Schlarb
  * Shengjing Zhu


Congratulations!
