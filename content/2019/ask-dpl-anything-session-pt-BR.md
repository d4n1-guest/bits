Title: Introdução ao novo DPL, pergunte-lhe qualquer coisa! 
Date: 2019-04-26 19:20
Tags: dpl,debian-meeting
Slug: ask-dpl-anything
Author: Jonathan Carter
Lang: pt-BR
Translator: Daniel Pimentel
Status: published

Nós temos um novo DPL! Em 21 de Abril de 2019, Sam Hartman iniciou seu mandato 
como o novo Líder do Projeto Debian (DPL).

Junte-se a nós no canal [#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting) do [OFTC](https://www.oftc.net/) na rede IRC em 10 de Maio de 
2019 às 10:00 (fuso horário UTC) para uma introdução ao novo DPL, e também
para ter a chance de perguntar qualquer coisa.

Seu apelídio no IRC deve ser registrado nesse canal. Consulte e [registre sua conta](https://www.oftc.net/Services/) na seção do site oftc para maiores informações sobre como registrar seu apelídio.

Planejamos muito mais sessões no IRC futuramente.

Você pode sempre consultar o [debian-meeting wiki page](https://wiki.debian.org/IRC/debian-meeting) para as últimas informações e cronograma atualizado.
