Title: 100 Paper cuts kick-off
Date: 2019-06-13 20:30
Tags: papercuts, qa
Slug: 100-papercuts-kickoff
Author: Jonathan Carter
Status: published


## Introduction

Is there a thorny bug in Debian that ruins your user experience? Something
*just* annoying enough to bother you but not serious enough to constitute
an RC bug? Are grey panels and slightly broken icon themes making
you depressed?

Then join the 100 papercuts project! A project to identify and fix the 100
most annoying bugs in Debian over the next stable release cycle. That also includes
figuring out how to identify and categorize those bugs and make sure that
they are actually fixable in Debian (or ideally upstream).

The idea of a papercuts project isn't new, Ubuntu did this some years ago
which added a good amount of polish to the system.

## Kick-off Meeting and DebConf BoF

On the 17th of June at 19:00 UTC we're kicking off an initial brainstorming
session on IRC to gather some initial ideas.

We'll use that to seed discussion at DebConf19 in Brazil during a
[BoF session](https://debconf19.debconf.org/talks/84-100-paper-cuts-kick-off/)
where we'll solidify those plans into something actionable.

## Meeting details

When: 2019-06-17, 19:00 UTC
Where: [#debian-meeting](https://webchat.oftc.net/?channels=#debian-meeting) channel
on the [OFTC](https://www.oftc.net/) IRC network

Your IRC nick needs to be registered in order to join the channel. Refer to the
[Register your account](https://www.oftc.net/Services/) section on the OFTC website
for more information on how to register your nick.

You can always refer to
the [debian-meeting wiki page](https://wiki.debian.org/IRC/debian-meeting)
for the latest information and up to date schedule.

Hope to see you there!
